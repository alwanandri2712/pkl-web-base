<html><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../css/style.default.css" id="theme-stylesheet"><link id="new-stylesheet" rel="stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.png?3">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  <style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="index.html" class="navbar-brand font-weight-bold text-uppercase text-base">Dashboard</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">hi,learning</a>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">

    <!-- sidebar -->
    <div id="sidebar" class="sidebar py-3">
      <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item">
                <a href="index.php" class="sidebar-link text-muted">
                  <i class="o-home-1 mr-3 text-gray"></i>
                    <span>Home Learning</span>
                </a>
              </li>
              <li class="sidebar-list-item"><a href="logout.php" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>
      </div>
    <!-- end sidebar -->
      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row">
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                  day 1
              <a href="day-1" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
                  <br>
              </div>

              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                  day 2
              <a href="day-2" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
                  <br>
              </div>

              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                  day 3
              <a href="day-3" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
                  <br>
              </div>

              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                  day 4
              <a href="day-4" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>
                  <br>
              </div>

              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 5
              <a href="day-5" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 6
              <a href="day-6" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 7
              <a href="day-7" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 8
              <a href="day-8" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 9
              <a href="day-9" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 10
              <a href="day-10" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 11
              <a href="day-11" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 12
              <a href="day-12" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 13
              <a href="day-13" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 14
              <a href="day-14" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 15
              <a href="day-15" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 16
              <a href="day-16" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 17
              <a href="day-17" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 18
              <a href="day-18" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 19
              <a href="day-19" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 20
              <a href="day-20" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>
              <div class="card-header" style="width: 150px; border-radius: 80px 80px 80px 80px; margin-left: 40px; margin-top: 25px;">
                day 21
              <a href="day-21" class="fa fa-arrow-right" aria-hidden="true" style="float: right; margin-right: 10px;"></a>

                  <br>
              </div>

     
    </div>
          </section>

        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">PKL TEAM © 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by <a href="https://bootstrapious.com/admin-templates" class="external text-gray-400">Bootstrapious</a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="../js/charts-home.js"></script>
    <script src="../js/front.js"></script>
  

</body></html>