<!-- COPYRIGHT BY PKL TEAM -->
<!-- License PKL TEAM Develop -->
<!-- Code by Alwan -->
<?php 
require '../../koneksi.php';
?> 

<?php
session_start();
if(!isset($_SESSION['username'])) {
  header('location:login.php');
} else {
  $username = $_SESSION['username'];
}

if($_SESSION['level']!="learning"){
  die("<script>alert('Anda Bukan User Learning ,Silahkan Back');</script>");
}

?>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../../css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../../css/style.default.css" id="theme-stylesheet"><link id="new-stylesheet" rel="stylesheet"><link id="new-stylesheet" rel="stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../../css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.png?3">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  <style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style><style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="index.html" class="navbar-brand font-weight-bold text-uppercase text-base">Dashboard</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">hi,learning</a>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">

    <!-- sidebar -->
    <div id="sidebar" class="sidebar py-3">
      <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item">
                <a href="index.php" class="sidebar-link text-muted">
                  <i class="o-home-1 mr-3 text-gray"></i>
                    <span>Home Learning</span>
                </a>
              </li>
              <li class="sidebar-list-item"><a href="../../logout.php" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>

      </div>
    <!-- end sidebar -->
      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="card border">
	   <div class="card-header">
      Day 15
    </div>
  	<div class="card-body">
     <br>
     <table style="width:100%;">
    <tbody>
    <?php
        $sql = "SELECT * FROM tbl_soal WHERE aktif='Y' AND day='15'   ORDER BY RAND () LIMIT 5";
        $query = mysqli_query($koneksi,$sql) or die (mysqli_error($koneksi));
        $jumlah = mysqli_num_rows($query);
        $no = 0;
        while($data = mysqli_fetch_array($query)){?>
					<form action="" method="post">
						<input type="hidden" name="id[]" value="<?php echo $data['id_soal']; ?>">
						<input type="hidden" name="jumlah" value="<?php echo $jumlah; ?>">

						<tr>
							<td><?php echo $no = $no+1; ?></td>
							<td ><?php echo $data['soal'];?></td>
						</tr>

						<!--  -->

						<tr>
							<td></td>
							<td>A. <input name="pilihan[<?php echo $data['id_soal']?>]" type="radio" value="A" required><?php echo $data['a'];?></td>
						</tr>
						<tr>
							<td></td>
							<td>B. <input name="pilihan[<?php echo $data['id_soal']?>]" type="radio" value="B" required> <?php echo $data['b'];?></td>
						</tr>
						<tr>
							<td></td>
							<td>C. <input name="pilihan[<?php echo $data['id_soal']?>]" type="radio" value="C" required> <?php echo $data['c'];?></td>
						</tr>
						<tr>
							<td></td>
							<td>D. <input name="pilihan[<?php echo $data['id_soal']?>]" type="radio" value="D" required> <?php echo $data['d'];?></td>
						</tr>
            <?php } ?>
            <button type="submit" name='submit' class="btn btn-primary float-right">SUBMIT</button>
						</form>
		    </tbody>
	    </table>
      <?php
        if(isset($_POST['submit'])){
          error_reporting(0);
          $pilihan = $_POST["pilihan"];
          $id_soal = $_POST["id"];
          $jumlah = $_POST["jumlah"];

          $score = 0;
          $benar = 0;
          $salah = 0;
          $kosong = 0;

          for($i=0;$i<$jumlah;$i++){
            $nomor = $id_soal[$i];
            if(empty($pilihan[$nomor])){
              $kosong++;
            } else {
              $jawaban = $pilihan[$nomor];
              $sql = "SELECT * FROM tbl_soal WHERE id_soal='$nomor' AND kunci='$jawaban'";
              $query = mysqli_query($koneksi,$sql) or die (mysqli_error($koneksi));
              $cek = mysqli_num_rows($query);

              if($cek){
                $benar++;
                
              } else {
                $false = true;
                $salah++;
              }

            }
            $sql = "SELECT * FROM tbl_soal WHERE aktif='Y'";
            $query = mysqli_query($koneksi,$sql) or die (mysqli_error($koneksi));
            $jumlah_soal = mysqli_num_rows($query);
            $score = 100 / $jumlah_soal * $benar;
            $hasil = number_format($score,1);
          }

          echo $false ? '<div align="center"><b>Anda Masih ada Salah Silahkan Cek Pdf => </b><a href="Soal_Day_13-15.pdf" class="btn btn-success">Click</a></div>' : '<script type="text/javascript">
          setTimeout(function () { 
            swal({
              title: "Score!", 
              text: "Jawaban Anda Benar Semua", 
              type: "info",
              confirmButtonText: "OK"
            }, function(isConfirm) {
                  if(isConfirm){
                    window.location = "../index.php";
                  }
              });},1000);
        </script>';
        }
        ?>
          </div>
       </div>
          </section>

        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">PKL TEAM © 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by <a href="https://bootstrapious.com/admin-templates" class="external text-gray-400">Bootstrapious</a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="../../js/charts-home.js"></script>
    <script src="../../js/front.js"></script>
  

</body></html>