  <?php
  include 'koneksi.php';
  session_start();

  // if($_SESSION['level'] == "admin"){
  //   //HTMLNYA ADMIN ?
  // }elseif ($_SESSION['level' == "user"]) {
  //   // HTML USER
  // }
  //
if(!isset($_SESSION['username'])) {
    header('location:login.php');
 } else {
    $username = $_SESSION['username'];
 }

if($_SESSION['level'] != "admin"){
    die("<script>alert('Anda Bukan Admin,Silahkan Back');</script>");
    // die('<script>alert("Anda Bukan Admin");window.location = "halaman-kartu";</script>');
  }

  ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.png?3">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="index.html" class="navbar-brand font-weight-bold text-uppercase text-base">Dashboard</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">hi,<?=$_SESSION['level']?></a>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">

    <!-- sidebar -->
    <div id="sidebar" class="sidebar py-3">
      <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item">
                <a href="index.php" class="sidebar-link text-muted">
                  <i class="o-home-1 mr-3 text-gray"></i>
                    <span>Home</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="manage_learning" class="sidebar-link text-muted">
                  <i class="o-table-content-1 mr-3 text-gray"></i>
                    <span>Manage Learning</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="manage_kartu" class="sidebar-link text-muted">
                  <i class="fa fa-id-card mr-3 text-gray"></i>
                  <span>Manage Cards</span>
                </a>
              </li>
              <li class="sidebar-list-item">
                <a href="manage_users" class="sidebar-link text-muted">
                  <i class="fa fa-users mr-3 text-gray"></i>
                  <span>Manage Users</span>
                </a>
              </li>
              <li class="sidebar-list-item"><a href="logout.php" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>

      </div>
    <!-- end sidebar -->

      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row">
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-violet"></div>
                    <div class="text">
                    <?php
                      $admin = mysqli_query($koneksi,"SELECT * FROM user WHERE level ='admin'");
                      $jumlah_admin = mysqli_num_rows($admin);
                    ?>
                      <h6 class="mb-0"> Admin </h6><span class="text-gray"><?= $jumlah_admin ?></span>
                    </div>
                  </div>
                  <div class="icon text-white bg-violet"><i class="fas fa-server"></i></div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-green"></div>
                    <div class="text">
                    <?php
                      $ppq = mysqli_query($koneksi,"select * from user where level ='learning'");
                      $jumlah_learning = mysqli_num_rows($ppq);
                    ?>
                      <h6 class="mb-0">User Learning</h6><span class="text-gray"><?php echo $jumlah_learning ?></span>
                    </div>
                  </div>
                  <div class="icon text-white bg-green"><i class="far fa-user"></i></div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-blue"></div>
                    <div class="text">
                    <?php
                      $kartu = mysqli_query($koneksi,"select * from user where level ='kartu'");
                      $jumlah_kartu = mysqli_num_rows($kartu);
                    ?>
                      <h6 class="mb-0">User Kartu</h6><span class="text-gray"><?= $jumlah_kartu ?></span>
                    </div>
                  </div>
                  <div class="icon text-white bg-blue"><i class="fa fa-user"></i></div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-red"></div>
                    <div class="text">
                    <?php
                      $total = mysqli_query($koneksi,"select * from user");
                      $jumlah_users = mysqli_num_rows($total);
                    ?>
                      <h6 class="mb-0">Total User</h6><span class="text-gray"><?= $jumlah_users ?></span>
                    </div>
                  </div>
                  <div class="icon text-white bg-red"><i class="fas fa-users"></i></div>
                </div>
              </div>
            </div>
          </section>

        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">PKL TEAM &copy; 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by <a href="https://bootstrapious.com/admin-templates" class="external text-gray-400">Bootstrapious</a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="js/charts-home.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>
