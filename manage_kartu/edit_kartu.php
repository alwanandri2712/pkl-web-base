<?php
include '../koneksi.php';
session_start();
if($_SESSION['level'] != "admin"){
  die("<script>alert('Anda Bukan Admin,Silahkan Back');</script>");
  // die('<script>alert("Anda Bukan Admin");window.location = "halaman-kartu";</script>');
}
if(!isset($_SESSION['username'])) {
  header('location:login.php');
} else {
  $username = $_SESSION['username'];
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../img/favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <?php include '../inc/navbar.php'; ?>
    </header>
    <div class="d-flex align-items-stretch">

      <?php include '../inc/sidebar.php'; ?>

      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row">
              <!-- Form Elements -->

              <div class="col-lg-12 mb-5">

                <div class="card">
                  <div class="card-header">
                    <h3 class="h6 text-uppercase mb-0">Edit Kartu</h3>
                  </div>
                  <div class="card-body">
                    <form class="form-horizontal" action="update_kartu.php" method="post">
                    <?php
                      $id = (int)$_GET['id_kartu'];
                      $result = $koneksi->query("SELECT * FROM tbl_kartu WHERE id_kartu='$id'");
                      while($users = $result->fetch_array())
                      {
                    ?>

                      <div class="form-group">
                        <label class="col-md-3 form-control-label">Gambar Kartu</label>
                        <div class="col-md-5">
                          <?php echo "<img style='width:250px;' src='../img/kartubiru/$users[gmbr_kartu]'>"?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 form-control-label">Judul Kartu</label>
                        <div class="col-md-5">
                          <input type="text" placeholder='Judul Kartu' class="form-control" name="judul_kartu" value='<?php echo $users['judul_kartu'];?>'>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3 form-control-label">Kata Kunci</label>
                        <div class="col-md-5">
                          <input type="text" placeholder='Kata Kunci' class="form-control" name="isi_kartu" value="<?php echo $users['isi_kartu'];?>">
                        </div>
                      </div>


                      <!-- <div class="line"></div> -->
                      <div class="form-group">
                        <div class="ml-auto">
                            <input type="hidden" name="id_kartu" value=<?php echo $_GET['id_kartu']; ?>>
                            <a href="index.php" class="btn btn-secondary">cancel</a>
                          <button type="submit" name="submit" class="btn btn-primary">Save</button>
                        </div>
                      </div>
                      <?php
                        }
                    ?>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <?php include '../inc/footer.php'; ?>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src=../vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="../js/front.js"></script>
  </body>
</html>
