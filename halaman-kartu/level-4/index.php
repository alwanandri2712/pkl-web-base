<?php
session_start();
if(!isset($_SESSION['username'])) {
  header('location:login.php');
} else {
  $username = $_SESSION['username'];
}

if($_SESSION['level'] != "kartu"){
  die("<script>alert('Anda Bukan User Kartu,Silahkan Back');</script>");
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-LEARNING - DASHBOARD</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../../css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../../css/style.default.css" id="theme-stylesheet"><link id="new-stylesheet" rel="stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../../css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../../img/favicon.png?3">
    <script src="../../vendor/js/sweetalert.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  <style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style></head>
  <body>
    <!-- navbar-->
    <header class="header">
            <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="index.html" class="navbar-brand font-weight-bold text-uppercase text-base">Dashboard</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">hi,kartu</a>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">


      <div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item">
                <a href="index.php" class="sidebar-link text-muted">
                  <i class="o-table-content-1 mr-3 text-gray"></i>
                    <span>Card Page</span>
                </a>
              </li>

              <li class="sidebar-list-item"><a href="../../logout.php" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>

      </div>


    <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-12">
          <br>
            <br>
            <div class="container">
              <div class="row align-items-center py-5">

          <div class="col-5 col-lg-5 mx-auto mb-7 mb-lg-0">
            <div class="pr-lg-5"><img src="Soalmerah1.png" alt="" class="img-fluid"></div>
          </div>
          <div class="col-lg-5 px-lg-4">
            <h2 class="mb-4">Jawaban</h2>
            <p class="text-muted">Jawablah soal berikut dengan kalimat yang tepat</p>
            <form id="loginForm" action="" method="post" class="mt-4">
              <div class="form-group mb-4">
                <input type="text" name="kalimat" placeholder="Tuliskan Kalimat" class="form-control border-0 shadow form-control-lg" autocomplete="off">
              </div>
              <div class="form-group mb-4">
              </div>
              <button type="submit" class="btn btn-primary shadow px-5">Check</button>
            </form>
            <?php
            if (isset($_POST['kalimat'])){
              include '../../koneksi.php';
              $kalimat = $_POST['kalimat'];
              // $result = $koneksi->query("SELECTa * from tbl_kartu");
              // $key1 = $result->fetch_array();
              $key1 =  ["sesuatu"];
              $exp = explode(" ",$kalimat);
              $status = false;
              foreach ($exp as $d) {
                $cek = in_array($d,$key1);
                if ($cek){
                  $status = true;
                  continue;
                }else{
                  continue;
                }
                break;
              }
              echo $status ? "
              <script> swal('Kalimat Benar!', 'Anda Bisa Next Ke Level Selanjutnya!', 'success')</script>
              <br>
              <a href='../index.php' class='btn btn-success'>Next Level
              <i class='fa fa-arrow-right' aria-hidden='true'></i>
              </a>" : "<script> swal('Oops!', 'Kalimat Salah!', 'error')</script>
              ";
            }
            ?>
                      </div>
        </div>
              <br>
              
            </div>
        </div>

        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">Intinya Alwan Ganteng © 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by <a href="https://bootstrapious.com/admin-templates" class="external text-gray-400">Bootstrapious</a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../../vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="../../js/front.js"></script>
  

</body></html>