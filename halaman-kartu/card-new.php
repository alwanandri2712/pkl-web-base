<?php
session_start();
if(!isset($_SESSION['username'])) {
  header('location:login.php');
} else {
  $username = $_SESSION['username'];
}

if($_SESSION['level'] != "kartu"){
  die("<script>alert('Anda Bukan User Kartu,Silahkan Back');</script>");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-LEARNING - DASHBOARD</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../img/favicon.png?3">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <?php include '../inc/navbar.php'; ?>
    </header>
    <div class="d-flex align-items-stretch">


      <div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item">
                <a href="card.php" class="sidebar-link text-muted">
                  <i class="o-table-content-1 mr-3 text-gray"></i>
                    <span>Card Page</span>
                </a>
              </li>

              <li class="sidebar-list-item"><a href="logout.php" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li>
        </ul>

      </div>


    <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-12">
          <br>
            <br>
            <div class="container">
              <div class="row">
                <div class="col-sm-2">
                  <a href="level-1"><button class="col-sm btn btn-info">Level 1</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-2"><button class="col-sm btn btn-info">Level 2</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-3"><button class="col-sm btn btn-info">Level 3</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-4"><button class="col-sm btn btn-info">Level 4</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-5"><button class="col-sm btn btn-info">Level 5</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-6"><button class="col-sm btn btn-info">Level 6</button></a>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-sm-2">
                  <a href="level-7"><button class="col-sm btn btn-info">Level 7</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-8"><button class="col-sm btn btn-info">Level 8</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-9"><button class="col-sm btn btn-info">Level 9</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-10"><button class="col-sm btn btn-info">Level 10</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-11"><button class="col-sm btn btn-info">Level 11</button></a>
                </div>
                <div class="col-sm-2">
                  <a href="level-12"><button class="col-sm btn btn-info">Level 12</button></a>
                </div>

              </div>
            </div>
        </div>

        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">Intinya Alwan Ganteng &copy; 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by <a href="https://bootstrapious.com/admin-templates" class="external text-gray-400">Bootstrapious</a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="../js/front.js"></script>
  </body>
</html>
