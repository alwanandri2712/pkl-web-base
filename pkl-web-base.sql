-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Mar 2020 pada 12.08
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pkl-web-base`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_kartu`
--

CREATE TABLE `tbl_kartu` (
  `id_kartu` int(11) NOT NULL,
  `gmbr_kartu` varchar(20) NOT NULL,
  `judul_kartu` varchar(50) NOT NULL,
  `isi_kartu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_kartu`
--

INSERT INTO `tbl_kartu` (`id_kartu`, `gmbr_kartu`, `judul_kartu`, `isi_kartu`) VALUES
(4, 'SoalBiru1.png', 'Negative Commands', 'beli'),
(6, 'SoalBiru2.png', 'TAG Question', 'sesuatu'),
(10, 'SoalBiru3.png', 'EMBEDDED COMMANDS', 'sekarang'),
(14, 'Soalbiru5.png', 'MODEL OPERATOR OF NECESSITY', 'tulis sendiri jan manja'),
(15, 'Soalbiru6.png', 'SUBORDINATE CLAUSE OF TIME', 'asdsd'),
(16, 'Soalbiru7.png', 'ADVERB & ADJECTIVE', 'asdsd'),
(17, 'Soalbiru8.png', 'AWARENESS OF PREDICATES', 'sdsd'),
(18, 'Soalbiru9.png', 'COMMENTARY ADVERB & ADJECTIVES', ''),
(19, 'Soalbiru10.png', 'MODEL OPERATOR OF NECESSITY', 'ASDASD');
-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_soal`
--

CREATE TABLE `tbl_soal` (
  `id_soal` int(11) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(50) NOT NULL,
  `b` varchar(50) NOT NULL,
  `c` varchar(50) NOT NULL,
  `d` varchar(50) NOT NULL,
  `kunci` varchar(50) NOT NULL,
  `day` int(5) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `aktif` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_soal`
--

INSERT INTO `tbl_soal` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `kunci`, `day`, `gambar`, `tanggal`, `aktif`) VALUES
(1, 'Berikut ini yang termasuk pertanyaan Outcomes adalah', 'Apa penyebab hal tersebut terjadi?', 'Apa yang menghentikan anda dalam menyelesaikannya?', 'Siapa yang mendapatkan keuntungan bila anda tidak ', 'Apa tujuan akhir dari coaching ini?', 'D', 1, '', '2018-07-02', 'Y'),
(2, 'Berikut ini yang termasuk pertanyaan Causes  adalah', 'Apa yang tidak berjalan dengan baik?', 'Apa yang ingin anda rubah hari ini?', 'Apa yang melatarbelakangi permasalahan tersebut?', 'Apa tujuan akhir dari coaching ini?', 'C', 1, '', '2018-07-02', 'Y'),
(3, 'Berikut ini yang termasuk pertanyaan Causes  adalah', 'Apa yang tidak berjalan dengan baik?', 'Apa yang ingin anda rubah hari ini?', 'Apa yang menghentikan anda dalam menyelesaikannya?', 'Apa tujuan akhir dari coaching ini?', 'C', 1, '', '2018-07-18', 'Y'),
(4, 'Berikut ini yang termasuk pertanyaan Symptoms adalah', 'Apa yang ingin anda rubah hari ini?', 'Apa penyebab hal tersebut terjadi?', 'Apa yang menghentikan anda dalam menyelesaikannya?', 'Apa tujuan akhir dari coaching ini?', 'A', 1, '', '2018-07-02', 'Y'),
(5, 'Berikut ini yang termasuk pertanyaan Symptoms adalah', 'Apa yang tidak berjalan dengan baik?', 'Apa penyebab hal tersebut terjadi?', 'Apa yang menghentikan anda dalam menyelesaikann', 'Siapa yang mendapatkan keuntungan bila anda tid', 'A', 1, '', '2018-07-02', 'Y'),
(6, 'Berikut ini yang termasuk pertanyaan Resources adalah', 'Apa yang anda mau?', 'Apa yang menghentikan anda dalam menyelesaikannya?', 'Siapa yang mendapatkan keuntungan bila anda tidak ', 'Apakah anda mempunyai pengalaman dimasa lalu yang ', 'D', 2, '', '0000-00-00', 'Y'),
(28, 'Berikut ini yang termasuk pertanyaan Outcomes adalah', 'Apa penyebab hal tersebut terjadi?', 'Apa yang anda mau?', 'Apa yang menghentikan anda dalam menyelesaikannya?', 'Siapa yang mendapatkan keuntungan bila anda tidak ', 'B', 2, '', '0000-00-00', 'Y'),
(30, 'Berikut ini yang termasuk pertanyaan Resources adalah', 'Sumber daya apa yang dibutuhkan untuk menyelesaika', 'Apa tujuan akhir dari coaching ini?', 'Apa yang anda mau?', 'Apa yang melatarbelakangi permasalahan tersebut?', 'A', 2, '', '0000-00-00', 'Y'),
(31, 'Berikut ini yang termasuk pertanyaan Effects adalah', 'Apa yang melatarbelakangi permasalahan tersebut?', 'Apa tujuan akhir dari coaching ini?', 'Apa yang anda mau?', 'Apa yang akan anda lakukan untuk tercapainya Goal ', 'D', 2, '', '0000-00-00', 'Y'),
(32, 'Berikut ini yang termasuk pertanyaan Effects adalah', 'Bagaimana dengan mencapai tujuan tersebut dapat me', 'Apa tujuan akhir dari coaching ini?', 'Apa yang anda mau?', 'Apa yang menghentikan anda dalam menyelesaikannya?', 'A', 2, '', '0000-00-00', 'Y'),
(33, 'Masalah menggunakan strategi yang paling efektif adalah definisi dari', 'Magic of Success', 'Power of Success', 'Mental Imagery', 'Tote Strategy', 'A', 3, '', '0000-00-00', 'Y'),
(34, 'Berikut ini yang tidak termasuk dalam TOTE', 'Test', 'Operate', 'Time', 'Exit', 'C', 3, '', '0000-00-00', 'Y'),
(35, 'Meta Questions dikembangkan oleh...', 'Michele K. Evans', 'L. Michael Hall Ph.D.', 'Robert Dilts', 'Milton H. Erickson', 'B', 3, '', '0000-00-00', 'Y'),
(36, 'Cara yang ampuh untuk anchor atau merubah connect dan resources di situasi masa depan atau event tertentu (seperti olahraga) adalah definisi dari…', 'Magic of Success', 'Fast Phobia Cure', 'Future pacing', 'Swish Pattern', 'C', 3, '', '0000-00-00', 'Y'),
(37, 'Future pacing adalah jenis dari...', 'Mental Imagery', 'Magic of Success', 'Power of Success ', 'd.	Tote Strategy', 'A', 3, '', '0000-00-00', 'Y'),
(38, 'Kata penanda Negative Commands adalah', 'adanya sugesti langsung', 'apakah, mungkinkah, ingin  tahu,  penasaran', 'jangan & tidak', 'bisakah…? , dapatkah...? , apakah..? Adakah....?dl', 'C', 4, '', '0000-00-00', 'Y'),
(39, 'Kata penanda Embedded Commands adalah', 'adanya sugesti langsung', 'apakah, mungkinkah, ingin  tahu,  penasaran', 'jangan & tidak', 'bisakah…? , dapatkah...? , apakah..? Adakah....?dl', 'A', 4, '', '0000-00-00', 'Y'),
(47, 'Kata penanda Embedded Question', 'adanya sugesti langsung', 'apakah, mungkinkah, ingin  tahu,  penasaran', 'jangan & tidak', 'bisakah…? , dapatkah...? , apakah..? Adakah....?dl', 'B', 4, '', '0000-00-00', 'Y'),
(48, 'Kata penanda Conversational Postulates:', 'adanya sugesti langsung', 'apakah, mungkinkah, ingin  tahu,  penasaran', 'jangan & tidak', 'bisakah…? , dapatkah...? , apakah..? Adakah....?dl', 'D', 4, '', '0000-00-00', 'Y'),
(49, 'Menyesuaikan tubuh Anda untuk mendekati pergeseran postural orang lain Body adalah output dari…', 'Part Matching', 'Whole Body Matching', 'Half Body Matching', 'Head/Shoulders Angel Patterns', 'B', 4, '', '0000-00-00', 'Y'),
(50, 'Pacing secara konsisten atau gerakan tubuh tertentu: tangan bergerak cepat, mata berkedip cepat adalah output dari…', 'Part Matching', 'Whole Body Matching', 'Half Body Matching', 'Head/Shoulders Angel Patterns', 'A', 5, '', '0000-00-00', 'Y'),
(51, 'Menyamakan bagian atas atau bawah tubuh orang lain adalah output dari…', 'Part Matching', 'Whole Body Matching', 'Half Body Matching', 'Head/Shoulders Angel Patterns', 'C', 5, '', '0000-00-00', 'Y'),
(52, 'Menyamakan pose karakteristik yang orang lain berikan dengan menyamakan kepala, dan bahu dan kata predikat adalah output dari…', 'Part Matching', 'Whole Body Matching', 'Half Body Matching', 'Head/Shoulders Angel Patterns', 'D', 5, '', '0000-00-00', 'Y'),
(53, 'Berikut pertanyaan Chunking up kecuali…', 'Ini adalah contoh dari?', 'Untuk tujuan apa hal itu?', 'Apa niat tertinggi dari hal itu?', 'Apa spesifiknya?', 'D', 5, '', '0000-00-00', 'Y'),
(54, 'Berikut pertanyaan Chunking down yaitu …', 'Ini adalah contoh dari?', 'Untuk tujuan apa hal itu?', 'Apa niat tertinggi dari hal itu?', 'd.	Apa spesifiknya?', 'D', 5, '', '0000-00-00', 'Y'),
(57, 'Mengubah persepsi dengan mengambil potongan atau sekelompok informasi ke arah umum atau abstrak atau lebih ke arah spesifik atau rinci adalah arti dari…', 'Chunking Up', 'Chunking Down', 'Chunking ', 'd.	Chunking Sideway', 'C', 6, '', '0000-00-00', 'Y'),
(58, 'Kalimat yang kehilangan sebagian informasi disebut dengan…', 'Simple Deletion', 'Comparative Deletion', 'Lack Of Referential Index', 'd.	Unspecified Verb', 'A', 6, '', '0000-00-00', 'Y'),
(59, 'Kalimat perbandingan yang kehilangan pembanding disebut dengan…', 'Simple Deletion', 'Comparative Deletion', 'Lack Of Referential Index', 'Unspecified Verb', 'B', 6, '', '0000-00-00', 'Y'),
(60, 'Susunan prioritas apa yang penting bagi seseorang, yang dicari olehnya di sebuah konteks, misalnya pekerjaan, rumah tangga, hobby, dll disebut dengan…', 'a.	Meta Program', 'Meta Model', 'Value Criteria', 'd.	Meta Question ', 'C', 6, '', '0000-00-00', 'Y'),
(61, 'Yang termasuk stress response kecuali…', 'Feeling', 'Thinking', 'Choice', 'Rules', 'D', 6, '', '0000-00-00', 'Y'),
(75, 'Yang termasuk dalam 4 MAT System kecuali…', 'What', 'why', 'When', 'What If', 'C', 7, '', '0000-00-00', 'Y'),
(76, 'Agar orang yang diajak berkomunikasi memahami maksud dan tujuan percakapan dan tidak terjadi biasa dalam berkomunikasi dengan orang lain adalah tujuan dari…', 'Reframing', 'Framing', 'Agreement Frame', 'Inokulasi & Self Objection', 'B', 7, '', '0000-00-00', 'Y'),
(77, 'Sebuah teknik memunculkan keberatan SEBELUM KEBERATAN ITU MUNCUL dari orang lain adalah definisi dari…', 'Reframing', 'Framing', 'Agreement Frame', 'Inokulasi & Self Objection', 'D', 7, '', '0000-00-00', 'Y'),
(78, 'MENETRALISIR keberatan yang akan muncul dari orang yang diajak bicara dengan cara mengangkat keberatan terlebih dahulu adalah tujuan dari…', 'Reframing', 'Framing', 'Agreement Frame', 'Inokulasi & Self Objection', 'D', 7, '', '0000-00-00', 'Y'),
(79, 'Berikut kata-kata yang digunakan dalam agreement frame, kecuali…', 'Benar pendapat anda dan ……..', 'Saya tidak setuju dan …….', 'Saya menghargai pemikiran anda tentang hal ini dan', 'Saya setuju dengan pemikiran yang bapak sampaikan ', 'B', 7, '', '0000-00-00', 'Y'),
(84, 'Digunakan untuk membingkai ulang pemikiran dan memberikan arti yang berbeda adalah tujuan dari', 'Reframing', 'Framing', 'Agreement Frame', 'Inokulasi & Self Objection', 'A', 8, '', '0000-00-00', 'Y'),
(86, 'Context Reframing adalah…', 'sebuah sudut pandang yang berbeda dari sebuah pern', 'sebuah sudut pandang yang berbeda dari sebuah pern', 'apa positif dari perilaku dan tujuan itu? ', 'Apa arti lain dari perlaku itu?', 'A', 8, '', '0000-00-00', 'Y'),
(87, 'Content Reframing adalah…', 'sebuah sudut pandang yang berbeda dari sebuah pern', 'sebuah sudut pandang yang berbeda dari sebuah pern', 'apa positif dari perilaku dan tujuan itu? Apa arti', 'Dalam konteks apa perilaku itu berguna?', 'C', 8, '', '0000-00-00', 'Y'),
(88, 'Conversational Postulates adalah…', 'Menghaluskan perintah', 'Menyembunyikan kalimat perintah dalam bentuk laran', 'Memberikan kata perintah dengan kata tanya', 'Memberikan kata perintah yang disembunyikan dalam ', 'D', 8, '', '0000-00-00', 'Y'),
(89, 'Embedded Commands adalah…', 'Menghaluskan perintah', 'Menghaluskan perintah', 'Memberikan kata perintah dengan kata tanya', 'Memberikan kata perintah yang disembunyikan dalam ', 'D', 8, '', '0000-00-00', 'Y'),
(90, 'Embedded Question adalah…', 'Menghaluskan perintah', 'Menyembunyikan kalimat perintah dalam bentuk laran', 'Memberikan kata perintah dengan kata tanya', 'Memberikan kata perintah yang disembunyikan dalam ', 'C', 9, '', '0000-00-00', 'Y'),
(91, 'Negative Commands adalah…', 'Menghaluskan perintah', 'Menyembunyikan kalimat perintah dalam bentuk laran', 'Memberikan kata perintah dengan kata tanya', 'Memberikan kata perintah yang disembunyikan dalam ', 'B', 9, '', '0000-00-00', 'Y'),
(92, 'Ketika masuk kelas ini, lepaskan atribut anda dan kita semua sejajar dan bermain dengan santai adalah contoh dari kalimat…', 'Reframing', 'Framing', 'Agreement Frame', 'Inokulasi & Self Objection', 'B', 9, '', '0000-00-00', 'Y'),
(93, 'Digunakan untuk membingkai pikiran seseorang, adalah kegunaan dari…', 'Reframing', 'Framing', 'Agreement Frame', 'Inokulasi & Self Objection', 'B', 9, '', '0000-00-00', 'Y'),
(94, 'Kebutuhan, Alasan, bukti nyata & hal yang Relevan merupakan sebuah unsur 4 MAT System yaitu…', 'What', 'Why', 'When', 'What  if', 'B', 9, '', '0000-00-00', 'Y'),
(95, 'Hal yang terjadi bila masalah tersebut sudah terselesaikan disebut dengan…', 'Symptoms', 'Causes', 'Outcomes', 'Effects', 'D', 10, '', '0000-00-00', 'Y'),
(96, 'Sumber-sumber yang digunakan untuk menyelesaikan masalah disebut dengan…', 'Symptoms', 'Causes', 'Resources ', 'Effects', 'C', 10, '', '0000-00-00', 'Y'),
(97, 'Masalah yang timbul disebut dengan…', 'Symptoms', 'Causes', 'Resources ', 'Effects', 'B', 10, '', '0000-00-00', 'Y'),
(99, 'Penyebab dari masalah tersebut disebut dengan…', 'Symptoms', 'Causes', 'Resources ', 'Effects', 'B', 10, '', '0000-00-00', 'Y'),
(100, 'Digunakan untuk menyusun dan merencanakan apa yang diinginkan disebut dengan…', 'Framing', 'Agreement Frame', 'Inokulasi & Self Objection', 'Well – Formed Goals', 'D', 10, '', '0000-00-00', 'Y'),
(101, 'Berikut kalimat Well – Formed Goals, kecuali', 'Dinyatakan dengan kalimat negatif. Hindari kata iy', '100% dapat dikontrol oleh Anda.', 'Ekologis – kondisi yang diciptakan adalah win – wi', 'Goal yang Anda tulis dapat di imajinasikan dengan ', 'A', 11, '', '0000-00-00', 'Y'),
(102, 'Gagasan logis dan penilaian kognitif yang kita bawa ke Gagasan lain disebut…', 'Feelings', 'Thoughts', 'Frames', 'Beliefs', 'B', 11, '', '0000-00-00', 'Y'),
(104, 'Gagasan emosional dan penilaian dengan perasaan yang kita bawa ke Gagasan lain disebut…', 'Feelings', 'Thoughts', 'Frames', 'Beliefs', 'A', 11, '', '0000-00-00', 'Y'),
(105, 'Gagasan yang kita tegaskan, validasikan, dan konfirmasikan disebut…', 'Feelings', 'Thoughts', 'Frames', 'Beliefs', 'D', 11, '', '0000-00-00', 'Y'),
(106, 'Gagasan yang kita gunakan untuk mengatur kerangka acuan berpikir kita, struktur konteks dalam pikiran kita disebut…', 'Feelings', 'Thoughts', 'Frames', 'Beliefs', 'C', 11, '', '0000-00-00', 'Y'),
(107, 'Dapat digunakan untuk mengkoding keunggulan seseorang (modeling) adalah fungsi dari…', 'TOTE Strategy', 'Inokulasi & Self Objection', 'Well – Formed Goals', 'Reframing', 'A', 12, '', '0000-00-00', 'Y'),
(108, 'Gagasan yang kita hargai, yang kita anggap penting dan berarti, harga diri disebut…', 'Value/Importance', 'Meanings', 'Appreciation', 'Intention', 'A', 12, '', '0000-00-00', 'Y'),
(109, 'Gagasan yang kita miliki tentang niat kita disebut…', 'Value/Importance', 'Meanings', 'Appreciation', 'Intention', 'D', 12, '', '0000-00-00', 'Y'),
(110, 'Gagasan penghargaan yang kita gunakan untuk membingkai Gagasan lain disebut…', 'Value/Importance', 'Meanings', 'Appreciation', 'Intention', 'C', 12, '', '0000-00-00', 'Y'),
(111, 'Gagasan yang ada di pikiran kita disebut…', 'Value/Importance', 'Meanings', 'Appreciation', 'Intention', 'B', 12, '', '0000-00-00', 'Y'),
(112, 'Teknik untuk membuat keputusan atas dua pilihan yang kadang membingungkan disebut dengan…', 'Resources - Visual Squash', 'Six Step Re-framing', 'Timeline – Building Better Future', 'Spining', 'A', 13, '', '0000-00-00', 'Y'),
(113, 'Proses keempat dalam Resources – Visual Squash yaitu…', 'Ditangan yang satu (tangan kanan misalnya) imajina', 'Buat representasi sistem yang jelas dengan sensory', 'Imajinasikan sebuah problem state ditangan yang sa', 'Buat representasi sistem yang jelas baik secara se', 'B', 13, '', '0000-00-00', 'Y'),
(114, 'Proses ketiga dalam Resources – Visual Squash yaitu…', 'Ditangan yang satu (tangan kanan misalnya) imajina', 'Buat representasi sistem yang jelas dengan sensory', 'Imajinasikan sebuah problem state ditangan yang sa', 'Buat representasi sistem yang jelas baik secara se', 'A', 13, '', '0000-00-00', 'Y'),
(115, 'Proses pertama dalam Resources – Visual Squash yaitu…', 'Ditangan yang satu (tangan kanan misalnya) imajina', 'Buat representasi sistem yang jelas dengan sensory', 'Imajinasikan sebuah problem state ditangan yang sa', 'Buat representasi sistem yang jelas baik secara se', 'D', 13, '', '0000-00-00', 'Y'),
(116, 'Langkah pertama dalam Sub Modalities : Belief Change adalah…', 'Munculkan belief yang dulu anda anggap benar, seka', 'Munculkan Limiting belief yang anda miliki', 'Rubah dan geser submodalities', 'Pikirkan satu belief yang anda yakini kebenarannya', 'B', 14, '', '0000-00-00', 'Y'),
(118, 'Langkah kedua dalam Sub Modalities : Belief Change adalah…', 'bMunculkan Limiting belief yang anda miliki', 'Munculkan Limiting belief yang anda miliki', 'Rubah dan geser submodalities', 'Pikirkan satu belief yang anda yakini kebenarannya', 'A', 14, '', '0000-00-00', 'Y'),
(119, 'Langkah pertama dalam Swish Pattern adalah…', 'Pikirkan sejenak apa yang anda inginkan jika bisa ', 'Pilih kebiasaan compulsion yang mau dihilangkan', 'Bawa gambar compulsion tersebut terang dan tempatk', 'Lakukan proses ini secepatnya hingga 5 kali dan bu', 'B', 14, '', '0000-00-00', 'Y'),
(120, 'Langkah kedua dalam Swish Pattern adalah…', 'Pikirkan sejenak apa yang anda inginkan jika bisa ', 'Pilih kebiasaan compulsion yang mau dihilangkan', 'Bawa gambar compulsion tersebut terang dan tempatk', 'Lakukan proses ini secepatnya hingga 5 kali dan bu', 'A', 14, '', '0000-00-00', 'Y'),
(122, 'Langkah ketiga dalam Swish Pattern adalah…', 'Pikirkan sejenak apa yang anda inginkan jika bisa ', 'Pilih kebiasaan compulsion yang mau dihilangkan', 'Bawa gambar compulsion tersebut terang dan tempatk', 'Lakukan proses ini secepatnya hingga 5 kali dan bu', 'C', 14, '', '0000-00-00', 'Y'),
(125, 'Memahami peta orang lain untuk bisa menjadi lebih fleksibel dan kreatif Mempunyai sudut pandang yang berbeda disebut dengan …', 'Resources - Visual Squash', 'Six Step Re-framing', 'Sub Modalities : Belief Change', 'Perceptual Position', 'D', 15, '', '0000-00-00', 'Y'),
(126, 'Ada berapa posisi perceptual position?', '3', '4', '5', '6', 'B', 15, '', '0000-00-00', 'Y'),
(127, 'Sebuah teknik untuk mengakses dan mengakses ulang representasi sistem dengan titik tertentu yang menjadi bagian untuk dipicu disebut dengan…', 'Anchoring', 'Perceptual Position', 'Six Step Re-framing', 'Reframing', 'A', 15, '', '0000-00-00', 'Y'),
(128, 'Untuk memantapkan dan memastikan dari Present state ke Desire state. Bermanfaat untuk coaching dan planning merupakan tujuan dari teknik …', 'Resources - Visual Squash', 'Six Step Re-framing', 'Perceptual Position', 'Visual Squash (New)', 'D', 15, '', '0000-00-00', 'Y'),
(129, 'Beberapa hal penting untuk diingat tentang penahan dalam Anchoring, kecuali…', 'Anchor perlu dikondisikan selama jangka waktu yang', 'Anchor akan kuat tanpa adanya reward langsung atau', 'Pengalaman internal dianggap signifikan dan dapat ', 'Pengalaman yang intens dapat diatur sesuai waktu y', 'A', 15, '', '0000-00-00', 'Y'),
(130, 'Teknik yang digunakan untuk berkomunikasi dengan bagian diri mencapai keseimbangan disebut dengan teknik…', 'Resources - Visual Squash', 'Six Step Re-framing', 'Timeline – Building Better Future', 'Spining', 'B', 16, '', '0000-00-00', 'Y'),
(131, 'Step ketiga dalam Six Step Re-framing yaitu…', 'Bangun komunikasi dengan bagian tersebut.', 'Pisah kan niat atau intense dari perilaku.b.	Pisah', 'Ciptakan perilaku penganti yang tetap dapat memuas', 'Ekological check.', 'B', 16, '', '0000-00-00', 'Y'),
(133, 'Untuk membawa perilaku yang berguna untuk mencapai tujuan adalah fungsi dari…', 'Resources - Visual Squash', 'Six Step Re-framing', 'Timeline – Building Better Future', 'New Behavior Generator', 'D', 16, '', '0000-00-00', 'Y'),
(134, 'Step keenam dalam Six Step Re-framing yaitu…', 'Bangun komunikasi dengan bagian tersebut.', 'Pisah kan niat atau intense dari perilaku.', 'Ciptakan perilaku penganti yang tetap dapat memuas', 'Ekological check.', 'D', 16, '', '0000-00-00', 'Y'),
(135, 'Step ketiga dalam New Behavior Generator yaitu…', 'Identifikasikan perilaku baru apa yang ingin anda ', 'Pastikan outcome tersebut sensory based dan apakah', 'Silahkan anda pejamkan mata untuk memulai proses i', 'Ciptakan disosiasi seperti anda sedang menonton fi', 'C', 16, '', '0000-00-00', 'Y'),
(136, 'Step keempat dalam New Behavior Generator yaitu…', 'Identifikasikan perilaku baru apa yang ingin anda ', 'Pastikan outcome tersebut sensory based dan apakah', 'Silahkan anda pejamkan mata untuk memulai proses i', 'Ciptakan disosiasi seperti anda sedang menonton fi', 'D', 17, '', '0000-00-00', 'Y'),
(137, 'Apa kegunaan dari Timeline – Building Better Future?', 'untuk membuat keputusan atas dua pilihan yang kada', 'untuk melihat hambatan dengan menggunakan garis ke', 'untuk ketika klien merasa tidak berdaya karena pen', 'untuk membawa perilaku yang berguna untuk mencapai', 'B', 17, '', '0000-00-00', 'Y'),
(138, 'Dapat digunakan untuk menetralkan perasaan negatif dan untuk meningkatkan perasaan positif dengan menggunakan visualisasi putaran adalah manfaat dari', 'Dynamic Spining', 'Perceptual Position', 'Anchoring', 'Fast Phobia Cure', 'A', 17, '', '0000-00-00', 'Y'),
(139, 'Proses pertama dalam Dynamic Spining adalah…', 'Letakan perasaan tersebut ditanggan anda.', 'Membayangkan dan merasakan kembali perasaan tidak ', 'Coba untuk memvisualisasikan jika perasaan tersebu', 'Berikan makna tertentu pada putaran tersebut hingg', 'B', 17, '', '0000-00-00', 'Y'),
(140, 'Proses kedua dalam Dynamic Spining adalah…', 'Letakan perasaan tersebut ditanggan anda.', 'Membayangkan dan merasakan kembali perasaan tidak ', 'Coba untuk memvisualisasikan jika perasaan tersebu', 'Berikan makna tertentu pada putaran tersebut hingg', 'A', 17, '', '0000-00-00', 'Y'),
(141, 'Proses ketiga dalam Dynamic Spining adalah…', 'Letakan perasaan tersebut ditanggan anda.', 'Membayangkan dan merasakan kembali perasaan tidak ', 'Coba untuk memvisualisasikan jika perasaan tersebu', 'Berikan makna tertentu pada putaran tersebut hingg', 'C', 18, '', '0000-00-00', 'Y'),
(142, 'Proses kelima dalam Dynamic Spining adalah…', 'Berikan makna tertentu pada putaran tersebut hingg', 'Putarlah kearah yang berlawanan kemudian apakah ya', 'Putaran dihentikan ketika Anda sudah merasa lebih ', 'Membayangkan dan merasakan kembali perasaan tidak ', 'B', 18, '', '0000-00-00', 'Y'),
(143, 'Proses terakhir dalam Dynamic Spining adalah…', 'Berikan makna tertentu pada putaran tersebut hingg', 'Putarlah kearah yang berlawanan kemudian apakah ya', 'Putaran dihentikan ketika Anda sudah merasa lebih ', 'Semakin cepat dan besar maka Anda dapat memasukan ', 'D', 18, '', '0000-00-00', 'Y'),
(145, 'Teknik apa yang dapat digunakan untuk memperkuat hasil dari Fast Phobia Cure?', 'Presuposisi Milton Model', 'Dynamic Spining', 'Perceptual Position', 'Anchoring', 'A', 18, '', '0000-00-00', 'Y'),
(146, 'Value Criteria adalah…', 'Memahami peta orang lain untuk bisa menjadi lebih ', 'Sebuah teknik untuk mengakses dan mengakses ulang ', 'Untuk memantapkan dan memastikan dari Present stat', 'Susunan prioritas apa yang penting bagi seseorang,', 'B', 18, '', '0000-00-00', 'Y'),
(147, 'Cara yang ampuh untuk anchor atau merubah connect dan resources di situasi masa depan atau event tertentu (seperti olahraga) adalah ', 'Future pacing', 'Anchoring Resources to Contexts', 'Changing Personal History', 'Building Better Future', 'A', 19, '', '0000-00-00', 'Y'),
(148, 'Langkah pertama dalam Future Pacing to Test adalah', 'Pikirkan empat situasi yang mungkin di masa depan,', 'Bayangkan melangkah ke situasi pertama. Lihat, den', 'Temukan ketakutan yang selama ini menganggu dia.', 'Biarkan dirinya masuk kedalam imaginary movie thea', 'A', 19, '', '0000-00-00', 'Y'),
(149, 'Langkah kedua dalam Future Pacing to Test adalah', 'Pikirkan empat situasi yang mungkin di masa depan,', 'Bayangkan melangkah ke situasi pertama. Lihat, den', 'Temukan ketakutan yang selama ini menganggu dia.', 'Biarkan dirinya masuk kedalam imaginary movie thea', 'B', 19, '', '0000-00-00', 'Y'),
(150, 'Langkah keempat dalam Future Pacing to Test adalah', 'Pikirkan empat situasi yang mungkin di masa depan,', 'Pertimbangkan jika Anda perlu melakukan perubahan ', 'Temukan ketakutan yang selama ini menganggu dia.', 'Biarkan dirinya masuk kedalam imaginary movie thea', 'B', 20, '', '0000-00-00', 'Y'),
(151, 'Anchoring Resources to Contexts bertujuan untuk…', 'anchor atau merubah connect dan resources di situa', 'anchor atau sumber daya yang terhubung secara alam', 'ketika klien merasa tidak berdaya karena pengalama', 'melihat hambatan dengan menggunakan garis kehidupa', 'B', 20, '', '0000-00-00', 'Y'),
(152, 'Digunakan untuk ketika klien merasa tidak berdaya karena pengalaman masalalu (bukan trauma) merupakan tujuan dari', 'Future pacing', 'Anchoring Resources to Contexts', 'Changing Personal History', 'Building Better Future', 'C', 20, '', '0000-00-00', 'Y'),
(153, 'Teknik Changing Personal History dapat dikombinasi dengan teknik…', 'Timeline – Building Better Future', 'Future Pacing', 'Timeline – kinestetik', 'Changing personal', 'C', 21, '', '0000-00-00', 'Y'),
(155, 'Future pacing adalah jenis dari', 'Mental Imagery', 'Magic of Success', 'Power of Success ', 'Tote Strategy', 'A', 21, '', '0000-00-00', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(20) NOT NULL,
  `nm_lengkap` varchar(255) NOT NULL,
  `no_tlpn` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `email`, `nm_lengkap`, `no_tlpn`, `username`, `password`, `level`) VALUES
(16, 'learning@email.com', 'learning', 23728738, 'learning', 'learning', 'learning'),
(17, 'admin@admin.com', 'admina', 121212, 'admin', 'admin', 'admin'),
(18, 'kartu@email.com', 'kartu', 323323, 'kartu', 'kartu', 'kartu'),
(19, 'alwanputra2712@gmail', 'alwan putra', 2147483647, 'owner', '123', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_kartu`
--
ALTER TABLE `tbl_kartu`
  ADD PRIMARY KEY (`id_kartu`);

--
-- Indeks untuk tabel `tbl_soal`
--
ALTER TABLE `tbl_soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_kartu`
--
ALTER TABLE `tbl_kartu`
  MODIFY `id_kartu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_soal`
--
ALTER TABLE `tbl_soal`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
