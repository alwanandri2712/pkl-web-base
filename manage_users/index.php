<?php
session_start();

if(!isset($_SESSION['username'])) {
  header('location:login.php');
} else {
  $username = $_SESSION['username'];
}

if($_SESSION['level'] != "admin"){
  die("<script>alert('Anda Bukan Admin,Silahkan Back');</script>");
  // die('<script>alert("Anda Bukan Admin");window.location = "halaman-kartu";</script>');
}

include '../koneksi.php';

$result = $koneksi->query("SELECT * from user ORDER BY id DESC");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MANAGE - DASHBOARD</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../img/favicon.png">
    <script src="../vendor/jquery/jquery.min.js"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <?php include '../inc/navbar.php'; ?>
    </header>
    <div class="d-flex align-items-stretch">

      <?php include '../inc/sidebar.php'; ?>

      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row">
                <div class="card">
                  <div class="card-header">
                    <h6 class="text-uppercase mb-0">Manage Users<a class="float-sm-right btn-sm btn-success" data-toggle="modal" data-target="#myModal" style='border-radius:2px;'><i class="fas fa-plus" style='color:white;'></i></a></h6>
                  </div>
                  <!-- modal -->
                  <form action="add_users.php" method="post">
                  <dis id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                      <div role="document" class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 id="exampleModalLabel" class="modal-title">Tambah Users</h4>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                          </div>
                          <!-- form Modal -->
                          <div class="modal-body">
                            <div class="form-group">
                              <?php
                                  $a = [
                                  'Pilih Role:' => '',
                                  'admin' => 'admin',
                                  'learning' => 'learning',
                                  'kartu' => 'kartu',
                                  ];
                              ?>
                              <select class="form-control" style="border-radius:15px;" name="nama_role" required>
                              <?php foreach ($a as $key => $value):?>
                                  <option value="<?= $value ?>"><?= $key ?></option>
                              <?php endforeach ?>
                              </select>
                              </div>
                              <div class="form-group">
                                <label>Email</label>
                                <input type="text" placeholder="Email Address" name='email' class="form-control">
                              </div>
                              <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" placeholder="Full Name" name='nm_lengkap' class="form-control">
                              </div>
                              <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" placeholder="Phone Number" name='no_tlpn' class="form-control">
                              </div>
                              <div class="form-group">
                                <label>Username</label>
                                <input type="text" placeholder="Username" name='username' class="form-control">
                              </div>
                              <div class="form-group">
                                <label>Password</label>
                                <input type="password" placeholder="Password" name='password' class="form-control">
                              </div>
                              </form>
                              <!-- <div class="form-group">
                                <input type="submit" value="Signin" class="btn btn-primary">
                              </div> -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                            <button type="submit" class="btn btn-primary" name='submit'>Simpan</button>
                          </div>
                        </div>
                      </div>
                    </dis>
                  </form>
                    <!-- End Modal -->
                  <div class="card-body">
                    <div class="table-responsive">
                    <table id="example1" class="table">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Email</th>
                          <th class='text-nowrap'>Nama Lengkap</th>
                          <th class='text-nowrap'>Phone Number</th>
                          <th>Username</th>
                          <th>Password</th>
                          <th>Level</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php
                          $no = 1;
                            while($users = $result->fetch_array())
                            {
                              echo "<td>".$no."</td>";
                              echo "<td>".$users['email']."</td>";
                              echo "<td>".$users['nm_lengkap']."</td>";
                              echo "<td>".$users['no_tlpn']."</td>";
                              echo "<td>".$users['username']."</td>";
                              echo "<td>".$users['password']."</td>";
                              echo "<td>".$users['level']."</td>";
                              echo "
                              <td class='d-flex'>
                              <a class='btn btn-info btn-sm text-nowrap' href='edit_users.php?id=$users[id]'>
                              <i class='fas fa-edit'></i></a>&nbsp
                              <a class='btn btn-danger btn-sm text-nowrap'
                              href='hapus_users.php?id=$users[id]'
                              onclick='return confirm(`Yakin Delete?`)'
                              >
                              <i class='fas fa-trash'></i></a>
                              </td>
                              </tr>
                              ";
                              $no++;
                            }
                          ?>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>



            </div>
          </section>
        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">

          <?php include '../inc/footer.php'; ?>

        </footer>
      </div>
    </div>
    <script>
    $(function () {
      $("#example1").DataTable();
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
    });
    </script>
    <!-- JavaScript files-->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script src="../vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="../js/front.js"></script>
  </body>
</html>
